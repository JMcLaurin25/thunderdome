--CREATE DATABASE thunderdome;
--\c thunderdome

CREATE TABLE type (
		name TEXT PRIMARY KEY
	);

CREATE TABLE attack (
		id SERIAL PRIMARY KEY,
		name TEXT NOT NULL,
		type TEXT NOT NULL REFERENCES type(name),
		min_dmg INTEGER NOT NULL,
		max_dmg INTEGER NOT NULL,
		speed INTERVAL HOUR TO SECOND NOT NULL
	);

CREATE TABLE species (
		id SERIAL PRIMARY KEY,
		name TEXT NOT NULL,
		type TEXT NOT NULL REFERENCES type(name),
		base_atk INTEGER NOT NULL,
		base_dfn INTEGER NOT NULL,
		base_hp INTEGER NOT NULL
	);

CREATE TABLE combatant (
		id SERIAL PRIMARY KEY,
		name TEXT NOT NULL,
		species_id INTEGER NOT NULL REFERENCES species(id),
		plus_atk INTEGER NOT NULL,
		plus_dfn INTEGER NOT NULL,
		plus_hp INTEGER NOT NULL
	);

CREATE TABLE species_attack (
		species_id INTEGER NOT NULL REFERENCES species(id),
		attack_id INTEGER NOT NULL REFERENCES attack(id)
	);

CREATE TYPE e_winner AS ENUM (
                'One',
                'Two',
                'Tie');

CREATE TABLE fight (
		combatant_one INTEGER NOT NULL REFERENCES combatant(id),
		combatant_two INTEGER NOT NULL REFERENCES combatant(id),
		winner e_winner NOT NULL,
		start TIMESTAMP NOT NULL,
		finish TIMESTAMP NOT NULL
	);

-- type
INSERT INTO type (name)
 VALUES 
('Physical'),
('Biological'),
('Radioactive'),
('Chemical'),
('Technological'),
('Mystical'),
('Mineral');


-- species
-- sum() = 300
INSERT INTO species (name, type, base_atk, base_dfn, base_hp)
 VALUES
('human','Physical',100,100,100),
('zombie','Biological',50,110,140),
('mutant','Radioactive',110,140,50),
('ooze','Chemical',140,50,110),
('robot','Technological',50,140,110),
('wendigo','Mystical',140,110,50),
('xorn','Mineral',110,50,140),
('monkey','Physical',50,200,50),
('sentient offal','Biological',150,0,150),
('imaginary dragon','Radioactive',50,50,200),
('toxic revenger','Chemical',200,50,50),
('land mine','Technological',300,0,0),
('ghost','Mystical',150,150,0),
('large rock','Mineral',0,150,150);

-- (max + min) = 30*speed
-- attack
INSERT INTO attack (name, type, min_dmg, max_dmg, speed)
 VALUES
('punch','Physical',15,15,INTERVAL '1 second'),
('kick','Physical',20,40,INTERVAL '2 seconds'),
('suplex','Physical',30,90,INTERVAL '4 seconds'),
('venom bite','Biological',5,55,INTERVAL '2 seconds'),
('loogie','Biological',10,230,INTERVAL '8 seconds'),
('disease','Biological',15,285,INTERVAL '10 seconds'),
('alpha particle','Radioactive',0,30,INTERVAL '1 seconds'),
('beta particle','Radioactive',0,90,INTERVAL '3 seconds'),
('gamma particle','Radioactive',0,150,INTERVAL '5 seconds'),
('acid spit','Chemical',60,90,INTERVAL '5 seconds'),
('lye vomit','Chemical',90,120,INTERVAL '7 seconds'),
('hydroxic acid','Chemical',120,150,INTERVAL '9 seconds'),
('buzz saw','Technological',60,60,INTERVAL '4 seconds'),
('laser','Technological',120,120,INTERVAL '8 seconds'),
('thermonuclear warhead','Technological',240,240,INTERVAL '16 seconds'),
('magic missle','Mystical',10,20,INTERVAL '1 seconds'),
('fireball','Mystical',40,80,INTERVAL '4 seconds'),
('lightning bolt','Mystical',80,160,INTERVAL '8 seconds'),
('pocket sand','Mineral',30,60,INTERVAL '3 seconds'),
('very small rocks','Mineral',5,85,INTERVAL '3 seconds'),
('crushing boredom','Mineral',30000,30000,INTERVAL '2000 seconds'),
('explode','Physical',30,30,INTERVAL '2 seconds');

INSERT INTO species_attack
 VALUES
 (1, 1),
 (1, 2),
 (1, 4),
 (1, 5),
 (1, 6),
 (1, 8),
 (1, 9),
 (1, 10),
 (1, 11),
 (1, 13),
 (1, 20),
 (1, 21),
 (1, 22),
 (2, 4),
 (2, 5),
 (2, 6),
 (2, 7),
 (2, 8),
 (2, 12),
 (2, 13),
 (2, 15),
 (2, 22),
 (3, 1),
 (3, 2),
 (3, 5),
 (3, 6),
 (3, 7),
 (3, 8),
 (3, 9),
 (3, 13),
 (3, 18),
 (3, 19),
 (3, 20),
 (3, 21),
 (3, 22),
 (4, 7),
 (4, 8),
 (4, 9),
 (4, 10),
 (4, 14),
 (4, 15),
 (4, 16),
 (4, 17),
 (4, 18),
 (4, 19),
 (4, 20),
 (4, 21),
 (4, 22),
 (5, 1),
 (5, 2),
 (5, 7),
 (5, 8),
 (5, 9),
 (5, 12),
 (5, 19),
 (5, 21),
 (5, 22),
 (6, 3),
 (6, 9),
 (6, 10),
 (6, 11),
 (6, 12),
 (6, 14),
 (6, 19),
 (6, 20),
 (6, 21),
 (6, 22),
 (7, 1),
 (7, 7),
 (7, 8),
 (7, 9),
 (7, 10),
 (7, 11),
 (7, 12),
 (7, 16),
 (7, 17),
 (7, 21),
 (7, 22),
 (8, 1),
 (8, 2),
 (8, 3),
 (8, 4),
 (8, 10),
 (8, 11),
 (8, 12),
 (8, 15),
 (8, 16),
 (8, 17),
 (8, 22),
 (9, 1),
 (9, 2),
 (9, 7),
 (9, 8),
 (9, 9),
 (9, 10),
 (9, 15),
 (9, 16),
 (9, 17),
 (9, 18),
 (10, 1),
 (10, 2),
 (10, 3),
 (10, 4),
 (10, 8),
 (10, 9),
 (10, 13),
 (10, 14),
 (10, 15),
 (10, 16),
 (11, 2),
 (11, 3),
 (11, 4),
 (11, 5),
 (11, 9),
 (11, 10),
 (11, 11),
 (11, 12),
 (11, 16),
 (11, 17),
 (11, 18),
 (11, 22),
 (12, 2),
 (12, 3),
 (12, 7),
 (12, 8),
 (12, 9),
 (12, 10),
 (12, 11),
 (12, 19),
 (12, 20),
 (12, 21),
 (12, 22),
 (13, 6),
 (13, 7),
 (13, 8),
 (13, 9),
 (13, 13),
 (13, 14),
 (13, 15),
 (13, 19),
 (13, 20),
 (13, 21),
 (13, 22),
 (14, 4),
 (14, 11),
 (14, 12),
 (14, 13),
 (14, 14),
 (14, 19),
 (14, 21),
 (14, 22);

