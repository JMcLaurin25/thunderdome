#!/usr/bin/python3
# reset.py

import psycopg2
import sys


def main():
    game_db = sys.argv[1]

    # Connects to Database and clears the tables
    try:
        conn = psycopg2.connect(database=game_db)
        cur = conn.cursor()
        cur.execute("DELETE from fight")
        cur.execute("DELETE from combatant")

        # Resets the indexing of the table back to 1
        cur.execute("ALTER SEQUENCE combatant_id_seq RESTART;")
        cur.execute("UPDATE combatant SET id = DEFAULT;")

        print("\t-Tournament fight tables reset")

    except Exception as e:
        print("Failed to connect to database '{0}', {1}".format(game_db, e))
        exit(1)

    conn.commit()
    conn.close()

if __name__ == '__main__':
    reset_args = len(sys.argv)
    if (reset_args == 2):
        main()
    elif (reset_args < 2):
        print("Please provide database name.\nUsage: ./reset <db-name>")
    else:
        print("Too many arguments provided.\nUsage: ./reset <db-name>")
