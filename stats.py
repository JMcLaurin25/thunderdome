#!/usr/bin/python3
# stats.py

import psycopg2
import sys


def longest(results):
    """Outputs the database queries results for longest fights."""
    print("\n  - Longest timed fighter :  {1:15}(ID:{0:2}) at {2}".format(
        results[0][0],
        results[0][1],
        results[0][2]
        ))


def shortest(results):
    """Outputs the database queries results for shortest fights."""
    print("  - Shortest timed fighter:  {1:15}(ID:{0:2}) at {2}".format(
        results[-1][0],
        results[-1][1],
        results[-1][2]
        ))


def get_wins(connection):
    """Queries the database for win results"""
    sql_win_query = "SELECT id, wins FROM ("
    sql_win_query += "SELECT CASE "
    sql_win_query += "WHEN winner = 'One' THEN combatant_one "
    sql_win_query += "WHEN winner = 'Two' THEN combatant_two "
    sql_win_query += "end AS id, COUNT(*) AS wins FROM fight "
    sql_win_query += "GROUP BY id "
    sql_win_query += "ORDER BY wins desc) AS wins wHERE ID IS NOT NULL "

    # Connection to retrieve wins
    try:
        connection.execute(sql_win_query)
        win_results = connection.fetchall()
    except Exception as e:
        print("\n  -Failed to query win data. {0}".format(e), file=sys.stderr)
        exit(1)

    # Retrieve names
    name_query = "SELECT combatant.id, combatant.name, species.type, "
    name_query += "species.name FROM combatant JOIN species ON "
    name_query += "species_id=species.id where combatant.id = "
    name_query += str(win_results[0][0])

    try:
        connection.execute(name_query)
        winner_data = connection.fetchall()
    except Exception as e:
        print("\n  -Failed to query name data. {0}".format(e), file=sys.stderr)
        exit(1)

    # Results output
    win_statement = "\n  - Most Wins=  {4:2}:  {1:15}(ID:{0:2}) "
    win_statement += "Species|Type: ({2}|{3})"
    print(win_statement.format(
        winner_data[0][0],
        winner_data[0][1],
        winner_data[0][2],
        winner_data[0][3],
        win_results[0][1]
        ))


def get_losses(connection):
    """Queries the database for loss results"""
    sql_loss_query = "SELECT id, loss FROM ("
    sql_loss_query += "SELECT CASE WHEN winner = 'One' THEN combatant_two "
    sql_loss_query += "WHEN winner = 'Two' THEN combatant_one "
    sql_loss_query += "end AS id, COUNT(*) AS loss FROM fight "
    sql_loss_query += "GROUP BY id "
    sql_loss_query += "ORDER BY loss desc) AS loss WHERE ID IS NOT NULL"

    # Connection to retrieve losses
    try:
        connection.execute(sql_loss_query)
        loss_results = connection.fetchall()
    except Exception as e:
        print("\n -Failed to query loss data. {0}".format(e), file=sys.stderr)
        exit(2)

    name_query = "SELECT combatant.id, combatant.name, species.type, "
    name_query += "species.name FROM combatant JOIN species ON "
    name_query += "species_id=species.id where combatant.id = "
    name_query += str(loss_results[0][0])

    # Retrieve Names
    try:
        connection.execute(name_query)
        loser_data = connection.fetchall()
    except Exception as e:
        failure = "\n -Failed to query loss name data. {0}"
        print(failure.format(e), file=sys.stderr)
        exit(2)

    # Results Output
    loss_statement = "  - Most Losses={4:2}:  {1:15}(ID:{0:2}) "
    loss_statement += "Species|Type: ({2}|{3})"
    print(loss_statement.format(
        loser_data[0][0],
        loser_data[0][1],
        loser_data[0][2],
        loser_data[0][3],
        loss_results[0][1]
        ))


def most_attacks(connection):
    """Queries the database for shortest attacks that can occur the most
    frequently"""
    attack_query = "SELECT combatant.id, combatant.name, species.type, "
    attack_query += "attack.speed, attack.name FROM combatant INNER JOIN "
    attack_query += "species ON combatant.species_id=species.id INNER JOIN "
    attack_query += "attack ON species.type=attack.type WHERE attack.speed = "
    attack_query += "'1' ORDER BY combatant.id;"

    # Connection for Attacks query
    try:
        connection.execute(attack_query)
        attack_results = connection.fetchall()
    except Exception as e:
        failure = "\n -Failed to query attack data. {0}"
        print(failure.format(e), file=sys.stderr)
        exit(1)

    # Results Output
    print("\n  - Most possible attacks:")
    for attack in attack_results:
        print("\t{0:15}(ID:{1:2}) - [{2:15} ({3})]".format(
            attack[1],
            attack[0],
            attack[4],
            attack[2]
            ))


def species_data(connection):
    """Queries the database for wins, losses, and ties based off of the fight
    results and combatant tables"""

    species_query = "SELECT DISTINCT name FROM species ORDER BY name"

    win_query = "SELECT CASE WHEN fight.winner='One' THEN (SELECT"
    win_query += " species.name FROM combatant JOIN species ON "
    win_query += "species_id=species.id WHERE combatant.id=combatant_one) "
    win_query += "WHEN fight.winner='Two' THEN (SELECT species.name FROM "
    win_query += "combatant JOIN species ON species_id=species.id WHERE "
    win_query += "combatant.id=combatant_two) "
    win_query += "END AS Species, COUNT(*) as wins "
    win_query += "FROM fight "
    win_query += "WHERE winner != 'Tie' "
    win_query += "GROUP BY Species ORDER BY Species;"

    loss_query = "SELECT CASE WHEN fight.winner='One' THEN (SELECT "
    loss_query += "species.name FROM combatant JOIN species ON "
    loss_query += "species_id=species.id WHERE combatant.id=combatant_two) "
    loss_query += "WHEN fight.winner='Two' THEN (SELECT species.name FROM "
    loss_query += "combatant JOIN species ON species_id=species.id WHERE "
    loss_query += "combatant.id=combatant_one) "
    loss_query += "END AS Species, COUNT(*) as losses "
    loss_query += "FROM fight "
    loss_query += "WHERE winner != 'Tie' "
    loss_query += "GROUP BY Species ORDER BY Species;"

    tie_query = "SELECT (SELECT species.name FROM combatant JOIN species ON "
    tie_query += "species_id=species.id "
    tie_query += "WHERE combatant.id=one.combatant_one), count(*) FROM "
    tie_query += "(SELECT combatant_one FROM fight WHERE winner='Tie' "
    tie_query += "UNION all "
    tie_query += "SELECT combatant_two FROM fight WHERE winner='Tie') as one "
    tie_query += "GROUP BY name "
    tie_query += "ORDER BY name"

    # Execution of queries
    try:
        connection.execute(species_query)
        species_results = connection.fetchall()

        connection.execute(win_query)
        win_results = connection.fetchall()

        connection.execute(loss_query)
        loss_results = connection.fetchall()

        connection.execute(tie_query)
        tie_results = connection.fetchall()

    except Exception as e:
        failure = "\n -Failed to query species data. {0}"
        print(failure.format(e), file=sys.stderr)
        exit(1)

    wins = 0
    losses = 0
    ties = 0

    # Results Output
    print("\n  -{0:20}{1:>8}{2:>10}{3:>8}\n".format(
        "Species",
        "Win(s)",
        "Loss(es)",
        "Tie(s)")
        )

    for species in species_results:
        for win in win_results:
            if win[0] == species[0]:
                wins = win[1]

        for loss in loss_results:
            if loss[0] == species[0]:
                losses = loss[1]

        for tie in tie_results:
            if tie[0] == species[0]:
                ties = tie[1]

        print("   {0:20}{1:>8}{2:>7}{3:>8}".format(
            species[0],
            wins,
            losses,
            ties)
            )


def main():
    game_db = sys.argv[1]

    try:
        conn = psycopg2.connect(database=game_db)
        print("Opened database '{0}' successfully".format(game_db))
    except Exception as e:
        failure = "\n  -Failed to connect to databse. {0}"
        print(failure.format(e), file=sys.stderr)
        exit(1)

    cur = conn.cursor()
    # Find the fight times
    sql_query = "SELECT id, name, SUM(finish-start)"
    sql_query += "FROM combatant JOIN fight ON id=combatant_one OR"
    sql_query += " id=combatant_two GROUP BY id ORDER BY sum DESC;"

    try:
        cur.execute(sql_query)
        results = cur.fetchall()
    except Exception as e:
        failure = "\n  -Failed to query fight duration data. {0}"
        print(failure.format(e), file=sys.stderr)
        exit(1)

    while (True):
        print("\n\t-Statistics to print:")
        print("\t  1 - Longest Fight")
        print("\t  2 - Shortest Fight")
        print("\t  3 - Most Wins")
        print("\t  4 - Most Losses")
        print("\t  5 - Most possible attacks")
        print("\t  6 - Wins - Losses - Ties")
        print("\t  7 - ALL STATISTICS")
        print("\t  8 - Exit")

        selection = input("\nSelection: ")

        if selection.isdigit():
            selection = int(selection)

            if selection == 1:
                long_fight = longest(results)

            elif selection == 2:
                short_fight = shortest(results)

            elif selection == 3:
                get_wins(cur)

            elif selection == 4:
                get_losses(cur)

            elif selection == 5:
                most_attacks(cur)

            elif selection == 6:
                species_data(cur)

            elif selection == 7:
                long_fight = longest(results)
                short_fight = shortest(results)
                get_wins(cur)
                get_losses(cur)
                most_attacks(cur)
                species_data(cur)

            elif selection == 8:
                exit(0)
            else:
                print("\n  - Invalid selection")

    conn.close()

if __name__ == '__main__':
    reset_args = len(sys.argv)
    if (reset_args == 2):
        main()
    elif (reset_args < 2):
        print("Please provide database name.\nUsage: ./reset <db-name>")
    else:
        print("Too many arguments provided.\nUsage: ./reset <db-name>")
