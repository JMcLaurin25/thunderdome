#!/usr/bin/python3
# thunderdome.py

import datetime
from datetime import date, timedelta
import psycopg2
import random
import sys


def create_combatants(cur_cursor):
    ''' Generates new combatants and inserts them into the existing combatant
    table '''

    names = [
            "Dunkin",
            "Hershey",
            "Mars",
            "Starbuck",
            "Shooter",
            "Champ",
            "Marvin",
            "Wiley",
            "Bugs",
            "Apple",
            "Ugly",
            "Highspeed",
            "WaveRider",
            "Reed",
            "One-eyed-Jack",
            "Billy Jean",
            "The 'Punn'isher",
            "Apollo",
            "Weak Body"
            ]

    try:
        # Verify if fight table is currently holding results
        cur_cursor.execute("SELECT COUNT(name) FROM combatant")
        combat_log = cur_cursor.fetchall()
        if (combat_log[0][0] != 0):
            print("\t- Existing combatants currently stored")
            return(0)

        # Fighters
        cur_cursor.execute("SELECT id FROM species")
        fighters = cur_cursor.fetchall()
    except Exception as e:
        print("\n  -Failed to query data. {0}".format(e))
        exit(1)

    random.shuffle(fighters)

    for fighter in fighters:
        try:
            sql_insert_combatant = "INSERT INTO combatant (name, species_id,"
            sql_insert_combatant += " plus_atk, plus_dfn, plus_hp) VALUES (%s,"
            sql_insert_combatant += " %s, %s, %s, %s)"
            cur_cursor.execute(
                    sql_insert_combatant,
                    (random.choice(names),
                        fighter[0],
                        0,
                        0,
                        0)
                    )
        except Exception as e:
            print("\n  -Failed to INSERT data. {0}".format(e))
            exit(1)


def run_tournament(cur_cursor):
    ''' Runs the tournament combats and stores the statistics into the fight
    table '''

    sql_insert_winner = "INSERT INTO fight (combatant_one, combatant_two,"
    sql_insert_winner += " winner, start, finish) VALUES (%s, %s, %s, %s, %s)"

    sql_join_combatant_species = "SELECT type, base_atk, base_dfn, base_hp,"
    sql_join_combatant_species += " species_id, combatant.id, combatant.name"
    sql_join_combatant_species += " FROM combatant JOIN species ON"
    sql_join_combatant_species += " species_id=species.id"

    try:
        cur_cursor.execute(sql_join_combatant_species)
        fighters = cur_cursor.fetchall()

        # Verify if fight table is currently holding results
        cur_cursor.execute("SELECT COUNT(winner) FROM fight")
        combat_log = cur_cursor.fetchall()
        if (combat_log[0][0] != 0):
            print("\t- Existing tournament results currently stored in log")
            exit(0)

        attack_query = "SELECT type, min_dmg, max_dmg, speed, name FROM attack"

        cur_cursor.execute(attack_query)
        attacks = cur_cursor.fetchall()
    except Exception as e:
        print("\n  -Failed to query data. {0}".format(e))
        exit(1)

    # Iterates through all fighter to allow combat between all
    for fighter_1 in fighters:
        for fighter_2 in fighters:
            if fighter_1 is fighter_2:
                break
            elif (fighter_1[0] == 'Mineral') and (fighter_2[0] == 'Mineral'):
                fight_results = ('Tie', timedelta(0))
            else:
                fight_results = run_fight(fighter_1, fighter_2, attacks)

            try:

                cur_cursor.execute(
                        sql_insert_winner,
                        (
                            str(fighter_1[5]),
                            str(fighter_2[5]),
                            str(fight_results[0]),
                            str(datetime.datetime.now() - fight_results[1]),
                            str(datetime.datetime.now())
                            )
                        )
            except Exception as e:
                print("\n  -Failed to INSERT data. {0}".format(e))
                exit(1)

    print("\t- Tournament completed")


def get_damage(fighter_1, fighter_2, cur_attack):
    """Calculates and returns the damage quantity"""

    att_def_matrix_key = {
            "Physical": 0,
            "Biological": 1,
            "Radioactive": 2,
            "Chemical": 3,
            "Technological": 4,
            "Mystical": 5,
            "Mineral": 6
            }

    att_def_matrix = (
            (1, 2, 1, 0.5, 1, 1, 1),
            (1, 1, 2, 1, 2, 1, 1),
            (2, 1, 1, 0.5, 0.5, 2, 1),
            (1, 2, 0.5, 0.5, 0.5, 1, 1),
            (1, 1, 0.5, 2, 1, 1, 1),
            (0.5, 2, 2, 0.5, 0.5, 0.5, 1),
            (1, 1, 1, 1, 1, 1, 0)
            )

    attacker = att_def_matrix_key[fighter_1[0]]
    defender = att_def_matrix_key[fighter_2[0]]

    att_def_mult = att_def_matrix[attacker][defender]

    cur_attack_key = att_def_matrix_key[cur_attack[0]]
    cur_attack_mult = att_def_matrix[cur_attack_key][defender]

    damage = 0
    # Damage calculations
    if ((fighter_2[1] - fighter_1[1]) >= 0):
        damage = att_def_mult
        damage *= (random.randint(0, (fighter_2[1] - fighter_1[1])))

    damage += cur_attack_mult * (random.randint(cur_attack[1], cur_attack[2]))

    return damage


def run_fight(fighter_1, fighter_2, attacks):
    """Simulates the tournament battles and stores the results in fight
    table"""
    fight_time = timedelta(seconds=0)
    winner = None

    fighter_1_hp = fighter_1[3]
    fighter_2_hp = fighter_2[3]

    # Random selection of attacks
    cur_attack_1 = random.choice(attacks)
    cur_attack_2 = random.choice(attacks)
    attack_time_1 = cur_attack_1[3]
    attack_time_2 = cur_attack_2[3]

    # Battle loop until hp below zero
    while (fighter_1_hp >= 0) and (fighter_2_hp >= 0):
        # Measures attack speed increment to determine hit

        # Fighter 1
        if (attack_time_1 <= timedelta(seconds=0)):
            attack_damage = get_damage(fighter_1, fighter_2, cur_attack_1)
            fighter_2_hp -= attack_damage
            cur_attack_1 = random.choice(attacks)
            print("  {2} attacks with {0} -{1} hp".format(
                cur_attack_1[4],
                attack_damage,
                fighter_1[6])
                )
            attack_time_1 = cur_attack_1[3]

        # Fighter 2
        if (attack_time_2 <= timedelta(seconds=0)):
            attack_damage = get_damage(fighter_1, fighter_2, cur_attack_2)
            fighter_1_hp -= attack_damage
            cur_attack_2 = random.choice(attacks)
            print("  {2} attacks with {0} -{1} hp".format(
                cur_attack_2[4],
                attack_damage,
                fighter_2[6])
                )
            attack_time_2 = cur_attack_2[3]

        # Sets the findings
        if (fighter_1_hp < 0) and (fighter_2_hp < 0):
            print("\t-TIE MATCH!")
            winner = 'Tie'
            break
        elif fighter_1_hp < 0:
            print("\t-{0} Defeats {1}!\n".format(fighter_2[6], fighter_1[6]))
            winner = 'Two'
            break
        elif fighter_2_hp < 0:
            print("\t-{0} Defeats {1}!\n".format(fighter_1[6], fighter_2[6]))
            winner = 'One'
            break

        attack_time_1 = attack_time_1 - timedelta(seconds=1)
        attack_time_2 = attack_time_2 - timedelta(seconds=1)

        fight_time = fight_time + timedelta(seconds=1)

    return (winner, fight_time)


def main():
    game_db = sys.argv[1]

    try:
        conn = psycopg2.connect(database=game_db)
        conn.autocommit = True
        print("\t- Connect to {0} database successfull".format(game_db))

        cur = conn.cursor()
        cur.execute("SELECT name FROM attack ORDER BY max_dmg - min_dmg")
    except Exception as e:
        print("\n  -Failed to query data. {0}".format(e))
        exit(1)

    create_combatants(cur)
    run_tournament(cur)

    conn.close()

if __name__ == '__main__':
    reset_args = len(sys.argv)
    if (reset_args == 2):
        main()
    elif (reset_args < 2):
        print("Please provide database name.\nUsage: ./reset <db-name>")
    else:
        print("Too many arguments provided.\nUsage: ./reset <db-name>")
